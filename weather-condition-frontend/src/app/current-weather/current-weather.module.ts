import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CurrentWeatherRoutingModule } from './current-weather-routing.module';
import { CurrentWeatherComponent } from './current-weather.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [CommonModule, TranslateModule, CurrentWeatherRoutingModule, HttpClientModule],
  declarations: [CurrentWeatherComponent]
})
export class CurrentWeatherModule {}
