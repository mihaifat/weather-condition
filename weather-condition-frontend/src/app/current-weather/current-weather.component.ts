import { Component, OnInit } from '@angular/core';
import {WeatherConditionDTO} from '@app/api/models/WeatherConditionDTO';
import {WeatherConditionService} from '@app/api/services/weather-condition.service';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.scss']
})
export class CurrentWeatherComponent implements OnInit {

  public weatherCondition: WeatherConditionDTO[];

  constructor(private weatherConditionService: WeatherConditionService) {}

  ngOnInit() {}

  public onClick(zipCode: string) {
    this.weatherConditionService.getWeatherConditionByPostalCode(zipCode).subscribe(
      (weatherCondition: WeatherConditionDTO[]) => {
        this.weatherCondition = weatherCondition;
      });
  }

}
