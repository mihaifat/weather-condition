import { TemperatureDTO } from './temperatureDto';

export interface WeatherConditionDTO {
  LocalObservationDateTime: string;
  EpochTime: number;
  WeatherText: string;
  WeatherIcon: number;
  HasPrecipitation: boolean;
  PrecipitationType: string;
  IsDayTime: boolean;
  Temperature: {
    Metric: TemperatureDTO,
    Imperial: TemperatureDTO
  },
  MobileLink: string;
  Link: string;
}
