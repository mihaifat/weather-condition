export interface TemperatureDTO {
  Value: number;
  Unit: string;
  UnitType: number;
}
