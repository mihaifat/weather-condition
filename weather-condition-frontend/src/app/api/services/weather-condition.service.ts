import { Injectable } from '@angular/core';
import { WeatherConditionDTO } from '../models/WeatherConditionDTO';
import { environment } from '@env/environment.prod';
import { Observable } from 'rxjs';
import {HttpService} from '@app/core';

@Injectable()
export class WeatherConditionService {

  constructor(private httpService: HttpService) {
  }

  getWeatherConditionByPostalCode(postalCode: string): Observable<WeatherConditionDTO[]> {
    return this.httpService.request('GET', `${environment.serverUrl}/api/weather-condition/${postalCode}`);
  }

}
