import { NgModule } from '@angular/core';
import { WeatherConditionService } from './services/weather-condition.service';

@NgModule({
  providers: [WeatherConditionService]
})
export class ApiModule {

}
