const serverless = require('serverless-http');
const express = require('express');
const axios = require('axios');
const app = express();

const apiKey = "ilNNyW6UPmAhHjKpZHzGE5TYz8xAe97Z";

app.get('/api/weather-condition/:zipCode', (req, res) => {
    const zipCode = req.params.zipCode;
    const url1 = `http://dataservice.accuweather.com/locations/v1/postalcodes/search?apikey=${apiKey}&q=${zipCode}`;

    axios.get(url1).then((locationInformationResponse) => {
        const locationKey = locationInformationResponse.data[0].Key;
        const url2  = `http://dataservice.accuweather.com/currentconditions/v1/${locationKey}?apikey=${apiKey}`;

        axios.get(url2).then((currentConditionsResponse) => {
            res.send(currentConditionsResponse.data);
        });
    })

});

module.exports.handler = serverless(app);
